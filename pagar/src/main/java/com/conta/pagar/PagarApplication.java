package com.conta.pagar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PagarApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagarApplication.class, args);
	}

}
