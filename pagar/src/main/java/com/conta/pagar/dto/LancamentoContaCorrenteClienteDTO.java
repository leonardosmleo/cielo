package com.conta.pagar.dto;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.conta.pagar.dto.DomicilioBancarioDTO;
import com.sun.istack.NotNull;

import io.swagger.annotations.ApiParam;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class LancamentoContaCorrenteClienteDTO implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiParam("Id do Lançamento Conta Corrente do Cliente")
	private Long idLancamentoContaCorrenteCliente;
	
	@ApiParam("Id do Domicilio Bancario")
    private DomicilioBancarioDTO domicilioBancario;
	
	@ApiParam("Número da remessa")
	private Integer numeroRemessaBanco;
	
	@ApiParam("Nome da situação da remessa")
	private String nomeSituacaoRemessa;
	
	@ApiParam("Nome do tipo de operação")
	private String nomeTipoOperacao;
}
