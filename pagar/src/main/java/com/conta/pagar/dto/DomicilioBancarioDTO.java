package com.conta.pagar.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiParam;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class DomicilioBancarioDTO implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiParam("Id do Domicilio Bancario")
	private long idDomicilioBancario;
	
	@ApiParam("Código do banco")
	private Integer codigoBanco;

	@ApiParam("Número da Agência")
	private Integer numeroAgencia;
	
	@ApiParam("Conta corrente em string")
	private String numeroContaCorrente;
}
