package com.conta.pagar.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDate;
import io.swagger.annotations.ApiParam;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class ControleLancamentoDTO implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiParam("Id do Controle Lançamento")
	private Long idControleLancamento;
	
	@ApiParam("Lançamento")
    private LancamentoContaCorrenteClienteDTO lancamentoContaCorrenteCliente;
	
	@ApiParam("Data Efetiva do lançamento")
	private LocalDate dataEfetivaLancamento;
	
	@ApiParam("Data de Lançamento")
	private LocalDate dataLancamentoContaCorrenteCliente;
    
	@ApiParam("Número do evento")
	private BigInteger numeroEvento;
	
	@ApiParam("Descrição do grupo de pagamento")
	private String descricaoGrupoPagamento;
	
	@ApiParam("Código de indentificação unico")
	private String codigoIdentificadorUnico;

	@ApiParam("Nome do Banco")
	private String nomeBanco;
	
	@ApiParam("Quantidade de Lançamentos")
	private Integer quantidadeLancamentoRemessa;

	@ApiParam("Raiz do cnpj")
	private String numeroRaizCNPJ;

	@ApiParam("Sufixo  do cnpj")
	private String numeroSufixoCNPJ;
	
	@ApiParam("Valor da remessa")
	private Double valorLancamentoRemessa;
	
	@ApiParam("Momento do lançamento")
	private Timestamp dateLancamentoContaCorrenteCliente;
    
	@ApiParam("Momento da efetivação")
	private Timestamp dateEfetivaLancamento;
}
