package com.conta.pagar.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@DynamicUpdate
@DynamicInsert
@AllArgsConstructor
@NoArgsConstructor
@Table(name="domicilio_bancario")
public class DomicilioBancarioEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "id_domicilio_bancario")
	private Long idDomicilioBancario;
	
	@Basic(optional = false)
	@Column(name = "codigo_banco")
	private Integer codigoBanco;
	
	@Basic(optional = false)
	@Column(name = "numero_agencia")
	private Integer numeroAgencia;
	
	@NotNull
	@Basic(optional = false)
    @Column(name = "numero_conta_corrente", length = 50)
	private String numeroContaCorrente;
}
