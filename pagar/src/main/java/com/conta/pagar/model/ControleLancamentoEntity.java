package com.conta.pagar.model;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDate;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.sun.istack.NotNull;

import lombok.Data;

@Data
@Entity
@DynamicUpdate
@DynamicInsert
@Table(name="controle_lancamento")
public class ControleLancamentoEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "id_controle_lancamento")
	private Long idControleLancamento;
	
    @JoinColumn(name = "id_lancamento_conta_corrente_cliente", referencedColumnName = "id_lancamento_conta_corrente_cliente")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private LancamentoContaCorrenteClienteEntity lancamentoContaCorrenteCliente;
	
    @Basic(optional = false)
	@Column(name = "data_efetiva_lancamento")
	private LocalDate dataEfetivaLancamento;
    
    @Basic(optional = false)
	@Column(name = "data_lancamento_conta_corrente_cliente")
	private LocalDate dataLancamentoContaCorrenteCliente;
    
	@Basic(optional = false)
	@Column(name = "numero_evento")
	private BigInteger numeroEvento;
	
	@Basic(optional = false)
	@Column(name = "descricao_grupo_pagamento", length = 100)
	private String descricaoGrupoPagamento;
	
	@NotNull
	@Basic(optional = false)
    @Column(name = "codigo_identificador_unico", length = 2)
	private String codigoIdentificadorUnico;

	@Basic(optional = false)
    @Column(name = "nome_banco", length = 100)
	private String nomeBanco;
	
	@Basic(optional = false)
    @Column(name = "quantidade_lancamento_remessa")
	private Integer quantidadeLancamentoRemessa;
	
	@Basic(optional = false)
    @Column(name = "numero_raiz_cnpj", length = 8)
	private String numeroRaizCNPJ;

	@Basic(optional = false)
    @Column(name = "numero_sufixocnpj", length = 4)
	private String numeroSufixoCNPJ;
	
	@Basic(optional = false)
    @Column(name = "valor_lancamento_remessa")
	private Double valorLancamentoRemessa;
	
    @Basic(optional = false)
	@Column(name = "date_lancamento_conta_corrente_cliente")
	private Timestamp dateLancamentoContaCorrenteCliente;
    
    @Basic(optional = false)
	@Column(name = "date_efetiva_lancamento")
	private Timestamp dateEfetivaLancamento;
}
