package com.conta.pagar.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.sun.istack.NotNull;

import lombok.Data;

@Data
@Entity
@DynamicUpdate
@DynamicInsert
@Table(name="lancamento_conta_corrente_cliente")
public class LancamentoContaCorrenteClienteEntity {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "id_lancamento_conta_corrente_cliente")
	private Long idLancamentoContaCorrenteCliente;
	
    @JoinColumn(name = "id_domicilio_bancario", referencedColumnName = "id_domicilio_bancario")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private DomicilioBancarioEntity domicilioBancario;
	
	@Basic(optional = false)
	@Column(name = "numero_remessa_banco")
	private Integer numeroRemessaBanco;
	

	@Basic(optional = false)
    @Column(name = "nome_situacao_remessa", length = 50)
	private String nomeSituacaoRemessa;
	

	@Basic(optional = false)
    @Column(name = "nome_tipo_operacao", length = 50)
	private String nomeTipoOperacao;
}
