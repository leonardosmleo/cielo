package com.conta.pagar.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.conta.pagar.model.LancamentoContaCorrenteClienteEntity;

public interface LancamentoContaCorrenteClienteRepository extends JpaRepository<LancamentoContaCorrenteClienteEntity, Long>{
}
