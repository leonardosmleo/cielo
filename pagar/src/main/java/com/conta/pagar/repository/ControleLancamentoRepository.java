package com.conta.pagar.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.conta.pagar.model.ControleLancamentoEntity;

public interface ControleLancamentoRepository extends JpaRepository<ControleLancamentoEntity, Long>{
}
