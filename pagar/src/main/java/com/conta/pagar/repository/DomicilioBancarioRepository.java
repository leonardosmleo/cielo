package com.conta.pagar.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.conta.pagar.model.DomicilioBancarioEntity;

public interface DomicilioBancarioRepository extends JpaRepository<DomicilioBancarioEntity, Long>{
	public DomicilioBancarioEntity findByNumeroContaCorrente(String numeroContaCorrente);
	public DomicilioBancarioEntity findByNumeroContaCorrenteAndIdDomicilioBancarioNot(String numeroContaCorrente, Long idDomicilioBancario);
}
