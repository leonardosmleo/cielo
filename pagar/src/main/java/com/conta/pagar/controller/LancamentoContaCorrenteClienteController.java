package com.conta.pagar.controller;

import static org.springframework.http.HttpStatus.ACCEPTED;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.conta.pagar.dto.LancamentoContaCorrenteClienteDTO;
import com.conta.pagar.service.LancamentoContaCorrenteClienteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value="/cielo/")
@Api(value = "Lancamento Conta Corrente Cliente")
public class LancamentoContaCorrenteClienteController {
	private static final HttpStatus NOT_FOUND = HttpStatus.NOT_FOUND;
	private static final HttpStatus CONFLICT = HttpStatus.CONFLICT;
	LancamentoContaCorrenteClienteService lancamentoContaCorrenteClienteService;
	
	@Autowired
	public LancamentoContaCorrenteClienteController(LancamentoContaCorrenteClienteService lancamentoContaCorrenteClienteService) {
		this.lancamentoContaCorrenteClienteService = lancamentoContaCorrenteClienteService;
	}
	
	@ApiOperation(value = "Retorna toda a lista")
	@GetMapping(value = "lancamento-conta/all")
	public List<LancamentoContaCorrenteClienteDTO> findAll() {
		return this.lancamentoContaCorrenteClienteService.findAll();
	}
	
	@ApiOperation(value = "Retorna um item por id")
	@GetMapping(value = "lancamento-conta/{id}")
	public ResponseEntity<Object> find(@PathVariable Long id) {
		LancamentoContaCorrenteClienteDTO dto = this.lancamentoContaCorrenteClienteService.findById(id);
		if(dto == null) {
			return new ResponseEntity<>("Lançamento não localizado", NOT_FOUND);
		}
		return new ResponseEntity<>(dto, ACCEPTED);
	}
	
	@ApiOperation(value = "Insere um registro")
	@PostMapping(value = "lancamento-conta/")
	public ResponseEntity<Object> insert(@RequestBody LancamentoContaCorrenteClienteDTO lancamentoContaCorrenteClienteDTO) {
		try {
			final LancamentoContaCorrenteClienteDTO novoDto = this.lancamentoContaCorrenteClienteService.insert(lancamentoContaCorrenteClienteDTO);
			return new ResponseEntity<>(novoDto, ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), NOT_FOUND);
		}
	}
	
	@ApiOperation(value = "Altera um registro por id")
	@PutMapping(value = "lancamento-conta/{id}")
	public ResponseEntity<Object> update(@PathVariable Long id, @RequestBody LancamentoContaCorrenteClienteDTO lancamentoContaCorrenteClienteDTO) {
		try {
			if(this.lancamentoContaCorrenteClienteService.existsById(id)) {
				final LancamentoContaCorrenteClienteDTO novoDto = this.lancamentoContaCorrenteClienteService.updateDTO(id, lancamentoContaCorrenteClienteDTO);
				if(novoDto==null) {
					return new ResponseEntity<Object>("Número da conta não pode ser igual", CONFLICT);
				}
				return new ResponseEntity<>(novoDto, ACCEPTED);
			}
			return new ResponseEntity<>(lancamentoContaCorrenteClienteDTO, NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, NOT_FOUND);
		}
	}
	
	@ApiOperation(value = "Remove o registro por id")
	@DeleteMapping(value = "lancamento-conta/{id}")
	public ResponseEntity<Object> delete(@PathVariable Long id) {
		try {
			this.lancamentoContaCorrenteClienteService.delete(id);
		} catch (final Exception e) {
			return new ResponseEntity<>(e.getMessage(), CONFLICT);
		}
		return ResponseEntity.noContent().build();
	}
}
