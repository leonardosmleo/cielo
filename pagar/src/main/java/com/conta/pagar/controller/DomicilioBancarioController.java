package com.conta.pagar.controller;

import static org.springframework.http.HttpStatus.ACCEPTED;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.conta.pagar.dto.DomicilioBancarioDTO;
import com.conta.pagar.service.DomicilioBancarioService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;

@Getter
@RestController
@RequestMapping(value="/cielo/")
@Api(value = "Domicilio Bancário")
public class DomicilioBancarioController {
	private static final HttpStatus NOT_FOUND = HttpStatus.NOT_FOUND;
	private static final HttpStatus CONFLICT = HttpStatus.CONFLICT;
	DomicilioBancarioService domicilioBancarioService;
	
	@Autowired
	public DomicilioBancarioController(DomicilioBancarioService domicilioBancarioService) {
		this.domicilioBancarioService = domicilioBancarioService;
	}
	
	@ApiOperation(value = "Retorna toda a lista")
	@GetMapping(value = "domicilio-bancario/all")
	public List<DomicilioBancarioDTO> findAll() {
		return this.domicilioBancarioService.findAll();
	}
	
	@ApiOperation(value = "Retorna um item por id")
	@GetMapping(value = "domicilio-bancario/{id}")
	public ResponseEntity<Object> find(@PathVariable Long id) {
		DomicilioBancarioDTO dto = this.domicilioBancarioService.findById(id);
		if(dto == null) {
			return new ResponseEntity<>("Domicilio não localizado", NOT_FOUND);
		}
		return new ResponseEntity<>(dto, ACCEPTED);
	}
	
	@ApiOperation(value = "Insere um registro")
	@PostMapping(value = "domicilio-bancario/")
	public ResponseEntity<Object> insert(@RequestBody DomicilioBancarioDTO domicilioBancarioDTO) {
		try {
			final DomicilioBancarioDTO novoDto = this.domicilioBancarioService.insert(domicilioBancarioDTO);
			if(novoDto==null) {
				return new ResponseEntity<Object>("Número da conta não pode ser igual", CONFLICT);
			}
			return new ResponseEntity<>(novoDto, ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, NOT_FOUND);
		}
	}
	
	@ApiOperation(value = "Altera um registro por id")
	@PutMapping(value = "domicilio-bancario/{id}")
	public ResponseEntity<Object> update(@PathVariable Long id, @RequestBody DomicilioBancarioDTO domicilioBancarioDTO) {
		try {
			if(this.domicilioBancarioService.existsById(id)) {
				final DomicilioBancarioDTO novoDto = this.domicilioBancarioService.updateDTO(id, domicilioBancarioDTO);
				if(novoDto==null) {
					return new ResponseEntity<Object>("Número da conta não pode ser igual", CONFLICT);
				}
				return new ResponseEntity<>(novoDto, ACCEPTED);
			}
			return new ResponseEntity<>(domicilioBancarioDTO, NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, NOT_FOUND);
		}
	}
	
	@ApiOperation(value = "Remove o registro por id")
	@DeleteMapping(value = "domicilio-bancario/{id}")
	public ResponseEntity<Object> delete(@PathVariable Long id) {
		try {
			this.domicilioBancarioService.delete(id);
		} catch (final Exception e) {
			return new ResponseEntity<>(e.getMessage(), CONFLICT);
		}
		return ResponseEntity.noContent().build();
	}
}
