package com.conta.pagar.controller;

import static org.springframework.http.HttpStatus.ACCEPTED;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.conta.pagar.dto.ControleLancamentoDTO;
import com.conta.pagar.service.ControleLancamentoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value="/cielo/")
@Api(value = "Controle de Lançamento")
public class ControleLancamentoController {
	private static final HttpStatus NOT_FOUND = HttpStatus.NOT_FOUND;
	private static final HttpStatus CONFLICT = HttpStatus.CONFLICT;
	ControleLancamentoService controleLancamentoService;
	
	@Autowired
	public ControleLancamentoController(ControleLancamentoService controleLancamentoService) {
		this.controleLancamentoService = controleLancamentoService;
	}
	
	@ApiOperation(value = "Retorna toda a lista")
	@GetMapping(value = "controle-lancamento/all")
	public List<ControleLancamentoDTO> findAll() {
		return this.controleLancamentoService.findAll();
	}
	
	@ApiOperation(value = "Retorna um item por id")
	@GetMapping(value = "controle-lancamento/{id}")
	public ResponseEntity<Object> find(@PathVariable Long id) {
		ControleLancamentoDTO dto = this.controleLancamentoService.findById(id);
		if(dto == null) {
			return new ResponseEntity<>("Lançamento não localizado", NOT_FOUND);
		}
		return new ResponseEntity<>(dto, ACCEPTED);
	}
	
	@ApiOperation(value = "Insere um registro")
	@PostMapping(value = "controle-lancamento/")
	public ResponseEntity<Object> insert(@RequestBody ControleLancamentoDTO controleLancamentoDTO) {
		try {
			final ControleLancamentoDTO novoDto = this.controleLancamentoService.insert(controleLancamentoDTO);
			return new ResponseEntity<>(novoDto, ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), NOT_FOUND);
		}
	}
	
	@ApiOperation(value = "Altera um registro por id")
	@PutMapping(value = "controle-lancamento/{id}")
	public ResponseEntity<Object> update(@PathVariable Long id, @RequestBody ControleLancamentoDTO controleLancamentoDTO) {
		try {
			if(this.controleLancamentoService.existsById(id)) {
				final ControleLancamentoDTO novoDto = this.controleLancamentoService.updateDTO(id, controleLancamentoDTO);
				if(novoDto==null) {
					return new ResponseEntity<Object>("Número da conta não pode ser igual", CONFLICT);
				}
				return new ResponseEntity<>(novoDto, ACCEPTED);
			}
			return new ResponseEntity<>(controleLancamentoDTO, NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, NOT_FOUND);
		}
	}
	
	@ApiOperation(value = "Remove o registro por id")
	@DeleteMapping(value = "controle-lancamento/{id}")
	public ResponseEntity<Object> delete(@PathVariable Long id) {
		try {
			this.controleLancamentoService.delete(id);
		} catch (final Exception e) {
			return new ResponseEntity<>(e.getMessage(), CONFLICT);
		}
		return ResponseEntity.noContent().build();
	}
}
