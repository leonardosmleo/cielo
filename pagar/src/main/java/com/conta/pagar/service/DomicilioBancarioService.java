package com.conta.pagar.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.conta.pagar.dto.DomicilioBancarioDTO;
import com.conta.pagar.model.DomicilioBancarioEntity;
import com.conta.pagar.repository.DomicilioBancarioRepository;

@Service
@Transactional
public class DomicilioBancarioService {
	@Autowired
	DomicilioBancarioRepository domicilioBancarioRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	public List<DomicilioBancarioDTO> findAll() {
		List<DomicilioBancarioEntity> listaEntity = domicilioBancarioRepository.findAll();
		List<DomicilioBancarioDTO> listaDTO = new ArrayList<DomicilioBancarioDTO>();
		if (listaEntity.size() > 0) {
			listaEntity.stream().forEach(x -> {
				listaDTO.add(modelMapper.map(x, DomicilioBancarioDTO.class));
			});
		}
		return listaDTO;
	}

	public DomicilioBancarioDTO findById(Long id) {
		Optional<DomicilioBancarioEntity> entity = domicilioBancarioRepository.findById(id);
		if(entity.isEmpty()) {
			return null;
		}
		return modelMapper.map(entity.get(), DomicilioBancarioDTO.class);
	}
	
	public DomicilioBancarioDTO update(DomicilioBancarioDTO domicilioBancarioDTO) {
		DomicilioBancarioEntity domicilioBancarioEntity = modelMapper.map(domicilioBancarioDTO, DomicilioBancarioEntity.class);
		return modelMapper.map(domicilioBancarioRepository.saveAndFlush(domicilioBancarioEntity), DomicilioBancarioDTO.class);
	}

	public DomicilioBancarioDTO updateDTO(Long id, DomicilioBancarioDTO domicilioBancarioDTO) {
		if(this.verificarContaCorrenteId(domicilioBancarioDTO.getNumeroContaCorrente(), domicilioBancarioDTO.getIdDomicilioBancario())) {
			return null;
		}
		return this.update(domicilioBancarioDTO);
	}
	
	public Boolean existsById(Long id) {
		return domicilioBancarioRepository.existsById(id);
	}


	public void delete(Long id) {
		domicilioBancarioRepository.deleteById(id);		
	}

	public DomicilioBancarioDTO insert(DomicilioBancarioDTO domicilioBancarioDTO) {

		if(this.verificarContaCorrente(domicilioBancarioDTO.getNumeroContaCorrente())) {
			DomicilioBancarioEntity entity =  domicilioBancarioRepository.save(modelMapper.map(domicilioBancarioDTO, DomicilioBancarioEntity.class));
			if(entity != null) {
				return modelMapper.map(entity,DomicilioBancarioDTO.class);
			}
		}
		return null;
	}

	public Boolean verificarContaCorrente(String numeroContaCorrente) {
		DomicilioBancarioEntity domicilioBancarioEntity =domicilioBancarioRepository.findByNumeroContaCorrente(numeroContaCorrente);
		if(domicilioBancarioEntity == null) {
			return Boolean.TRUE;
			
		}
		return Boolean.FALSE;
	}
	public Boolean verificarContaCorrenteId(String numeroContaCorrente, Long idDomicilioBancario) {
		DomicilioBancarioEntity domicilioBancarioEntity = domicilioBancarioRepository.findByNumeroContaCorrenteAndIdDomicilioBancarioNot(numeroContaCorrente, idDomicilioBancario);
		if(domicilioBancarioEntity == null) {
			return Boolean.TRUE;
			
		}
		return Boolean.FALSE;
	}
}
