package com.conta.pagar.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.conta.pagar.dto.ControleLancamentoDTO;
import com.conta.pagar.dto.DomicilioBancarioDTO;
import com.conta.pagar.dto.LancamentoContaCorrenteClienteDTO;
import com.conta.pagar.model.ControleLancamentoEntity;
import com.conta.pagar.model.DomicilioBancarioEntity;
import com.conta.pagar.model.LancamentoContaCorrenteClienteEntity;
import com.conta.pagar.repository.ControleLancamentoRepository;
import com.conta.pagar.repository.LancamentoContaCorrenteClienteRepository;

@Service
@Transactional
public class ControleLancamentoService {
	@Autowired
	ControleLancamentoRepository controleLancamentoRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	public List<ControleLancamentoDTO> findAll() {
		List<ControleLancamentoEntity> listaEntity = controleLancamentoRepository.findAll();
		List<ControleLancamentoDTO> listaDTO = new ArrayList<ControleLancamentoDTO>();
		if (listaEntity.size() > 0) {
			listaEntity.stream().forEach(x -> {
				listaDTO.add(modelMapper.map(x, ControleLancamentoDTO.class));
			});
		}
		return listaDTO;
	}

	public ControleLancamentoDTO findById(Long id) {
		Optional<ControleLancamentoEntity> entity = controleLancamentoRepository.findById(id);
		if(entity.isEmpty()) {
			return null;
		}
		return modelMapper.map(entity.get(), ControleLancamentoDTO.class);
	}
	
	public ControleLancamentoDTO update(ControleLancamentoDTO lancamentoContaCorrenteClienteDTO) {
		ControleLancamentoEntity lancamentoContaCorrenteClienteEntity = modelMapper.map(lancamentoContaCorrenteClienteDTO, ControleLancamentoEntity.class);
		return modelMapper.map(controleLancamentoRepository.saveAndFlush(lancamentoContaCorrenteClienteEntity), ControleLancamentoDTO.class);
	}

	public ControleLancamentoDTO updateDTO(Long id, ControleLancamentoDTO controleLancamentoDTO) {
		return this.update(controleLancamentoDTO);
	}
	
	public Boolean existsById(Long id) {
		return controleLancamentoRepository.existsById(id);
	}


	public void delete(Long id) {
		controleLancamentoRepository.deleteById(id);		
	}

	public ControleLancamentoDTO insert(ControleLancamentoDTO controleLancamentoDTO) {
		System.out.println("ity"+controleLancamentoDTO);
		ControleLancamentoEntity controleLancamentoEntity = modelMapper.map(controleLancamentoDTO, ControleLancamentoEntity.class);
		System.out.println(controleLancamentoEntity);
		ControleLancamentoEntity entity =  controleLancamentoRepository.save(controleLancamentoEntity);
		if(entity != null) {
			return modelMapper.map(entity,ControleLancamentoDTO.class);
		}
		return null;
	}

}
