package com.conta.pagar.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.conta.pagar.dto.DomicilioBancarioDTO;
import com.conta.pagar.dto.LancamentoContaCorrenteClienteDTO;
import com.conta.pagar.model.DomicilioBancarioEntity;
import com.conta.pagar.model.LancamentoContaCorrenteClienteEntity;
import com.conta.pagar.repository.LancamentoContaCorrenteClienteRepository;

@Service
@Transactional
public class LancamentoContaCorrenteClienteService {
	@Autowired
	LancamentoContaCorrenteClienteRepository lancamentoContaCorrenteClienteRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	public List<LancamentoContaCorrenteClienteDTO> findAll() {
		List<LancamentoContaCorrenteClienteEntity> listaEntity = lancamentoContaCorrenteClienteRepository.findAll();
		List<LancamentoContaCorrenteClienteDTO> listaDTO = new ArrayList<LancamentoContaCorrenteClienteDTO>();
		if (listaEntity.size() > 0) {
			listaEntity.stream().forEach(x -> {
				listaDTO.add(modelMapper.map(x, LancamentoContaCorrenteClienteDTO.class));
			});
		}
		return listaDTO;
	}

	public LancamentoContaCorrenteClienteDTO findById(Long id) {
		Optional<LancamentoContaCorrenteClienteEntity> entity = lancamentoContaCorrenteClienteRepository.findById(id);
		if(entity.isEmpty()) {
			return null;
		}
		return modelMapper.map(entity.get(), LancamentoContaCorrenteClienteDTO.class);
	}
	
	public LancamentoContaCorrenteClienteDTO update(LancamentoContaCorrenteClienteDTO lancamentoContaCorrenteClienteDTO) {
		LancamentoContaCorrenteClienteEntity lancamentoContaCorrenteClienteEntity = modelMapper.map(lancamentoContaCorrenteClienteDTO, LancamentoContaCorrenteClienteEntity.class);
		return modelMapper.map(lancamentoContaCorrenteClienteRepository.saveAndFlush(lancamentoContaCorrenteClienteEntity), LancamentoContaCorrenteClienteDTO.class);
	}

	public LancamentoContaCorrenteClienteDTO updateDTO(Long id, LancamentoContaCorrenteClienteDTO lancamentoContaCorrenteClienteDTO) {
		return this.update(lancamentoContaCorrenteClienteDTO);
	}
	
	public Boolean existsById(Long id) {
		return lancamentoContaCorrenteClienteRepository.existsById(id);
	}


	public void delete(Long id) {
		lancamentoContaCorrenteClienteRepository.deleteById(id);		
	}

	public LancamentoContaCorrenteClienteDTO insert(LancamentoContaCorrenteClienteDTO lancamentoContaCorrenteClienteDTO) {
		LancamentoContaCorrenteClienteEntity lancamentoEntity = modelMapper.map(lancamentoContaCorrenteClienteDTO, LancamentoContaCorrenteClienteEntity.class);

		LancamentoContaCorrenteClienteEntity entity =  lancamentoContaCorrenteClienteRepository.save(lancamentoEntity);
		if(entity != null) {
			return modelMapper.map(entity,LancamentoContaCorrenteClienteDTO.class);
		}
		return null;
	}

}
