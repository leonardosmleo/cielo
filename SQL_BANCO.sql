--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

-- Started on 2021-11-09 15:00:45

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 203 (class 1259 OID 16993)
-- Name: controle_lancamento; Type: TABLE; Schema: public; Owner: avaliacao
--

CREATE TABLE public.controle_lancamento (
    id_controle_lancamento bigint NOT NULL,
    codigo_identificador_unico character varying(2) NOT NULL,
    data_efetiva_lancamento date NOT NULL,
    data_lancamento_conta_corrente_cliente date NOT NULL,
    date_efetiva_lancamento timestamp without time zone NOT NULL,
    date_lancamento_conta_corrente_cliente timestamp without time zone NOT NULL,
    descricao_grupo_pagamento character varying(100) NOT NULL,
    nome_banco character varying(100) NOT NULL,
    numero_evento numeric(19,2) NOT NULL,
    numero_raiz_cnpj character varying(8) NOT NULL,
    numero_sufixocnpj character varying(4) NOT NULL,
    quantidade_lancamento_remessa integer NOT NULL,
    valor_lancamento_remessa double precision NOT NULL,
    id_lancamento_conta_corrente_cliente bigint NOT NULL
);


ALTER TABLE public.controle_lancamento OWNER TO avaliacao;

--
-- TOC entry 201 (class 1259 OID 16953)
-- Name: domicilio_bancario; Type: TABLE; Schema: public; Owner: avaliacao
--

CREATE TABLE public.domicilio_bancario (
    id_domicilio_bancario bigint NOT NULL,
    codigo_banco integer NOT NULL,
    numero_agencia integer NOT NULL,
    numero_conta_corrente character varying(50) NOT NULL
);


ALTER TABLE public.domicilio_bancario OWNER TO avaliacao;

--
-- TOC entry 200 (class 1259 OID 16920)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: avaliacao
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO avaliacao;

--
-- TOC entry 202 (class 1259 OID 16958)
-- Name: lancamento_conta_corrente_cliente; Type: TABLE; Schema: public; Owner: avaliacao
--

CREATE TABLE public.lancamento_conta_corrente_cliente (
    id_lancamento_conta_corrente_cliente bigint NOT NULL,
    nome_situacao_remessa character varying(50) NOT NULL,
    nome_tipo_operacao character varying(50) NOT NULL,
    numero_remessa_banco integer NOT NULL,
    id_domicilio_bancario bigint NOT NULL
);


ALTER TABLE public.lancamento_conta_corrente_cliente OWNER TO avaliacao;

--
-- TOC entry 2999 (class 0 OID 16993)
-- Dependencies: 203
-- Data for Name: controle_lancamento; Type: TABLE DATA; Schema: public; Owner: avaliacao
--

COPY public.controle_lancamento (id_controle_lancamento, codigo_identificador_unico, data_efetiva_lancamento, data_lancamento_conta_corrente_cliente, date_efetiva_lancamento, date_lancamento_conta_corrente_cliente, descricao_grupo_pagamento, nome_banco, numero_evento, numero_raiz_cnpj, numero_sufixocnpj, quantidade_lancamento_remessa, valor_lancamento_remessa, id_lancamento_conta_corrente_cliente) FROM stdin;
82	1	2016-06-03	2016-06-03	2016-06-03 00:00:00	2016-06-03 00:00:00	LA-56	BANCO ABCD S.A.             	42236790.00	12996721	1597	22	11499.1	40
\.


--
-- TOC entry 2997 (class 0 OID 16953)
-- Dependencies: 201
-- Data for Name: domicilio_bancario; Type: TABLE DATA; Schema: public; Owner: avaliacao
--

COPY public.domicilio_bancario (id_domicilio_bancario, codigo_banco, numero_agencia, numero_conta_corrente) FROM stdin;
11	1	2	61616116
27	2	44	5451146161
\.


--
-- TOC entry 2998 (class 0 OID 16958)
-- Dependencies: 202
-- Data for Name: lancamento_conta_corrente_cliente; Type: TABLE DATA; Schema: public; Owner: avaliacao
--

COPY public.lancamento_conta_corrente_cliente (id_lancamento_conta_corrente_cliente, nome_situacao_remessa, nome_tipo_operacao, numero_remessa_banco, id_domicilio_bancario) FROM stdin;
40	remessa	operacação	260	27
41	remessa	operacação	260	27
42	remessa	operacação	260	27
43	remessa	operacação	260	27
44	remessa	operacação	260	27
45	remessa	operacação	260	27
46	remessa	operacação	260	27
47	remessa	operacação	260	27
48	remessa	operacação	260	27
49	remessa	operacação	260	27
50	remessa	operacação	260	27
51	remessa	operacação	260	11
52	remessa	operacação	260	11
53	remessa	operacação	260	11
54	remessa	operacação	260	11
55	remessa 66	operacação	260	11
\.


--
-- TOC entry 3005 (class 0 OID 0)
-- Dependencies: 200
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: avaliacao
--

SELECT pg_catalog.setval('public.hibernate_sequence', 82, true);


--
-- TOC entry 2863 (class 2606 OID 16997)
-- Name: controle_lancamento controle_lancamento_pkey; Type: CONSTRAINT; Schema: public; Owner: avaliacao
--

ALTER TABLE ONLY public.controle_lancamento
    ADD CONSTRAINT controle_lancamento_pkey PRIMARY KEY (id_controle_lancamento);


--
-- TOC entry 2859 (class 2606 OID 16957)
-- Name: domicilio_bancario domicilio_bancario_pkey; Type: CONSTRAINT; Schema: public; Owner: avaliacao
--

ALTER TABLE ONLY public.domicilio_bancario
    ADD CONSTRAINT domicilio_bancario_pkey PRIMARY KEY (id_domicilio_bancario);


--
-- TOC entry 2861 (class 2606 OID 16962)
-- Name: lancamento_conta_corrente_cliente lancamento_conta_corrente_cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: avaliacao
--

ALTER TABLE ONLY public.lancamento_conta_corrente_cliente
    ADD CONSTRAINT lancamento_conta_corrente_cliente_pkey PRIMARY KEY (id_lancamento_conta_corrente_cliente);


--
-- TOC entry 2864 (class 2606 OID 16988)
-- Name: lancamento_conta_corrente_cliente fkej0cewer5d9boa5cx91es3vhn; Type: FK CONSTRAINT; Schema: public; Owner: avaliacao
--

ALTER TABLE ONLY public.lancamento_conta_corrente_cliente
    ADD CONSTRAINT fkej0cewer5d9boa5cx91es3vhn FOREIGN KEY (id_domicilio_bancario) REFERENCES public.domicilio_bancario(id_domicilio_bancario);


--
-- TOC entry 2865 (class 2606 OID 16998)
-- Name: controle_lancamento fkikbkfkjsf8fgam7hcipfshg9y; Type: FK CONSTRAINT; Schema: public; Owner: avaliacao
--

ALTER TABLE ONLY public.controle_lancamento
    ADD CONSTRAINT fkikbkfkjsf8fgam7hcipfshg9y FOREIGN KEY (id_lancamento_conta_corrente_cliente) REFERENCES public.lancamento_conta_corrente_cliente(id_lancamento_conta_corrente_cliente);


-- Completed on 2021-11-09 15:00:46

--
-- PostgreSQL database dump complete
--

